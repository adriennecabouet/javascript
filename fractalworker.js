/**
 * Created By: Adrienne Cabouet
 * Date: 8/20/13
 * Time: 10:41 PM
 */
importScripts("workerlib.js");
onmessage = function(task) {
    var workerResult = computeRow(task.data);
    postMessage(workerResult);
}