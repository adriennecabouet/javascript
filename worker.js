/**
 * Created By: Adrienne Cabouet
 * Date: 8/20/13
 * Time: 9:56 PM
 */
onmessage = pingPong;
function pingPong(event) {
    if (event.data == "ping") {
        postMessage("pong");
    }
}
