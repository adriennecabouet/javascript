/**
 * Created By: Adrienne Cabouet
 * Date: 8/20/13
 * Time: 9:51 PM
 */
window.onload = function (){
    var worker = new Worker("javascript/worker.js");

    worker.postMessage("ping");

    worker.onmessage = function (event) {
        var message = "Worker says " + event.data;

        document.getElementById("output").innerHTML = message;
    }
};
